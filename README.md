# CaC template documentation

Site: https://cac-template.gitlab.io/documentation

- [CaC template documentation](#cac-template-documentation)
  - [Description](#description)
  - [Contribute](#contribute)

## Description
This site is intended to keep accurate documentation on how kickstart continues compliance for your organization.

## Contribute

Install:
``` bash
python3 -m venv venv
source venv/bin/activate
pip3 install mkdocs-material
```

Run the server
``` bash
mkdocs serve
```



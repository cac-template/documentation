# Modify catalog

To add controls or modify a catalog.

1. Generate the markdown files
      1. `trestle author catalog-generate -n customCatalog -o markdowns/catalogs/customCatalog`
2. Modify or add markdown files
3. assemble json from catalog
      1. `trestle author catalog-assemble -m markdowns/catalogs/customCatalog -o customCatalog`



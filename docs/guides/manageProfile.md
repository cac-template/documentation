# Manage a profiles

The OSCAL Profile model represents a baseline of selected controls from one or more control catalogs, which is referred to as a "profile" in OSCAL. The Profile model is the sole model in the OSCAL Control layer.

![](https://pages.nist.gov/OSCAL/resources/concepts/terminology/profile-catalog-mapping-trivial-example.png)

## Create a profile

Recomended step is to create a profile for the org, this profile can import from multiple catalogs.
``` bash
trestle create --type profile -o <CustomName>
```
### Imports

Profiles and catalogs can referenced in profiles.

There is no command to add extra imports, we might need to change the json. In this example 2 catalogs and 1 profile are referenced.

From:
``` json
    "imports": [
      {
        "href": "trestle://catalogs/customCatalog"
      }
    ]
```

To:
``` json
    "imports": [
      {
        "href": "trestle://catalogs/customCatalog/catalog.json"
      },
      {
        "href": "trestle://catalogs/CIS_Controls_v8/catalog.json"
      },
      {
        "href": "trestle://profiles/NIST_SP-800-53_rev5_LOW-baseline/profile.json"
      }
    ]
```

Change the reference of the catalogs can be done by using the trestle command:
``` bash
trestle href -n <profile name> -hr trestle://catalogs/<catalog name>/catalog.json --item <import number>
```

Show your imports by just running `trestle href -n <profile name>`

![Profile import flow](profile_import_flow.svg)

Important note



## NIST Low baseline example

In this example `v1.0.0` is used, it is recommended to get the proper version as explained in the [catalog](catalogRelated.md) page.

``` bash
trestle import -f https://raw.githubusercontent.com/usnistgov/oscal-content/v1.0.0/nist.gov/SP800-53/rev5/json/NIST_SP-800-53_rev5_LOW-baseline_profile.json -o NIST_SP-800-53_rev5_LOW-baseline
```

![NIST example diagram](NIST_catalog_profiles.svg)

## CIS implementations groups

Implementation Groups (IGs) are the recommended guidance to prioritize implementation of the CIS Critical Security Controls (CIS Controls).

Read the following CIS links for more information:

* [CIS IG1](https://www.cisecurity.org/controls/implementation-groups/ig1)
* [CIS IG2](https://www.cisecurity.org/controls/implementation-groups/ig2)
* [CIS IG3](https://www.cisecurity.org/controls/implementation-groups/ig3)

Note: At the moment of writing this documenation I was not able to find a OSCAL profile specific to IG1.

I used the following commmand to get all the Safeguards part of the catalog and based on the information on IG 1 created the profile.

``` bash
jq '.. | objects | select(.controls? != null) | .controls[] | .id' catalog.json
```

![CIS example diagram](CIS_catalog_profiles.svg)

## Replicating a profile

```
trestle replicate profile -n NIST_SP-800-53_rev5_LOW-baseline -o Example-profile
```

# References
* [Latest profile reference](https://pages.nist.gov/OSCAL-Reference/models/latest/profile/)
* [Terminology](https://pages.nist.gov/OSCAL/resources/concepts/terminology/)
# Steps

1. Create directory
1. Install trestle
    *   
        ``` bash
        python3 -m venv venv
        source venv/bin/activate
        pip3 install compliance-trestle
        ```
1. Init trestle workspace: `trestle init`
2. [Import a catalog](catalogRelated.md)
      1. Optional: create a custom catalog.
3. Commit to git the imported OSCAL catalogs.
4. Import or create a [profile](manageProfile.md)
      1. Start with NIST low baseline or CIS IG1
5. Create a profile that references the required profiles and catalogs.
      1. Example: Custom catalog + CISv8 IG1 profile

# Import Catalogs

## Import NIST SP800-53

```
trestle import -f https://raw.githubusercontent.com/usnistgov/oscal-content/master/nist.gov/SP800-53/rev5/json/NIST_SP-800-53_rev5_catalog.json -o NIST_SP-800-53_rev5
```

If you face an issue regarding OSCAL version not supported, you need a catalog created with same or lower version of oscal.

Example:
``` bash
trestle.core.commands.import_:94 ERROR: Error while importing OSCAL file: OSCAL version: 1.1.1 is not supported, use 1.0.4 instead
```

We need version `1.0.4` or lower, for NIST we are going to check the tags, in tag `v1.0.0`, verify the oscal-version at the begining of the json file, notice that it shows `1.0.0`.

``` bash
trestle import -f https://raw.githubusercontent.com/usnistgov/oscal-content/v1.0.0/nist.gov/SP800-53/rev5/json/NIST_SP-800-53_rev5_catalog.json -o NIST_SP-800-53_rev5
```

## Import CIS

Search for CIS controls, it is easier to find them in the github repository:

[cis-controls-v8_OSCAL-1.0.xml](https://github.com/CISecurity/CISControls_OSCAL/blob/main/src/catalogs/xml/cis-controls-v8_OSCAL-1.0.xml)

This is an XML file, we need to translate to OSCAL json, use the format converter tool provided by NIST in this [link](https://pages.nist.gov/oscal-tools/demos/csx/format-converter/fromxml/).

Download the file and import using trestle.

Note: the path is just an example, replace with the name of the converted file and path.

``` bash
trestle import -f ~/Downloads/converted.json -o CIS_Controls_v8
```

### Alternative option
Download json or XML from [CIS workbench](https://workbench.cisecurity.org/files?q=OSCAL&tags=22)

# Get the list of safeguards in a catalog

``` bash
jq '.. | objects | select(.controls? != null) | .controls[] | .id' catalog.json
```



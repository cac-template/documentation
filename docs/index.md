![Colors used for diagrams](diagram_colors.svg)

![Authoring](authoring.svg)

## Trestle

[Trestle](https://oscal-compass.github.io/compliance-trestle/) is an ensemble of tools that enable the creation, validation, and governance of documentation artifacts for compliance needs. It leverages NIST's OSCAL as a standard data format for interchange between tools and people, and provides an opinionated approach to OSCAL adoption.




For full documentation visit [mkdocs.org](https://www.mkdocs.org).


